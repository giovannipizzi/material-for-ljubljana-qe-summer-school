This simulation uses the gamma point to sample the reciprocal space so you cannot exploit pool parallelism.

i. try to improve the time to solution using the `-ndiag` and `-ntg`.
ii. Try now reducing the number of MPI processes and increasing the OpenMP threads. Is the time to solution reduced?


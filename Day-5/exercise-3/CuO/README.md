# CuO

Complete the input file by setting a scratch directory and prepare a jobscript to use a single node.

Run the first simulation without any parallel parameter.

i. try to improve the time to solution by using the `-npool` option.
ii. try to further reduce the time to solution using the `-ntg` options.  Does the time to solution improve?
iii. try to reduce the number of MPI processes and exploit OpenMP parallelism. Does the time to solution improve?


